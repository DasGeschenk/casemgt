from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('Accounts/', include('Accounts.urls')),
    path('case_admin/', views.admin, name='admin'),
    path('matters/', views.openMatters, name='matters'),    
    path('contacts/', views.contactTemp, name='contacts'),
    path('feedback/', views.feedback, name='feedback'),
    path('user_dash/', views.user_dash, name='user_dash'),
    path('staff_dash/', views.staff_dash, name='staff_dash'),
]