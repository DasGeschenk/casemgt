from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView
from . import models
# Create your views here.
def index(request):
    return render(request, 'index.html')

# @login_required(login_url='/?next=case_admin')
def admin(request):
    return render(request, 'admin.html')

def openMatters(request):
    return render(request, 'matters.html')

def contactTemp(request):
    return render(request, 'contacts.html')

def feedback(request):
    if request.method == 'POST':
        feedback = models.Feedback(
            name=request.POST['contact-name'],
            email=request.POST['contact-email'],
            subject=request.POST['contact-Subject'],
            message=request.POST['contact-message']
        )
        feedback.save()
    return render(request, 'index.html')

def user_dash(request):
    return render(request, 'strathstd.html')  

def staff_dash(request):
    return render(request, 'strathstaff.html')
